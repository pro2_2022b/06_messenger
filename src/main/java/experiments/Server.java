package experiments;

import com.google.gson.*;
import experiments.api.GroupDTO;
import experiments.api.MessageDTO;
import experiments.api.UserDTO;
import experiments.persistence.*;
import experiments.utils.HttpRequest;
import experiments.utils.Utils;

import java.io.*;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Properties;

public class Server
{
    static Properties config;
    static Gson gson;

    public static void Listen() throws IOException
    {
        config = new Properties();
        config.load(new FileInputStream("app.config"));
        ServerSocket serverSocket = new ServerSocket(Integer.parseInt(config.getProperty("listeningPort")));
        GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Instant.class, new TimeAdapter());
        gson = gsonBuilder.create();
        while (true)
        {
            Socket socket = serverSocket.accept();
            Thread thread = new Thread(()-> {
                try {
                    Handle(socket);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            });
            thread.start();
        }
    }

    public static void Handle(Socket socket) throws IOException, SQLException, ClassNotFoundException {
        HttpRequest httpRequest = Utils.ReadHttpRequest(socket);


        DataAccess dataAccess = new SqlServerDataAccess(
                config.getProperty("dbServerUrl"),
                config.getProperty("dbUser"),
                config.getProperty("dbPassword")
        );
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());

        if(httpRequest.getPath().equals("/time") &&
                httpRequest.getMethod().equals("GET"))
        {
            printWriter.println("HTTP/1.1 200 OK"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/html; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(Instant.now()); // Zapiš HTTP tělo
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 1 &&
                httpRequest.getMethod().equals("GET") &&
                !httpRequest.getQueryPairs().containsKey("userId"))
        {
            GroupPersistence[] groups = dataAccess.GetGroups();
            ArrayList<GroupDTO> groupDTOS = new ArrayList<>();
            for(GroupPersistence g : groups)
            {
                groupDTOS.add(new GroupDTO(g.getName(),g.getId()));
            }
            String result = gson.toJson(groupDTOS);
            printWriter.println("HTTP/1.1 200 OK"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 1 &&
                httpRequest.getMethod().equals("GET")&&
                httpRequest.getQueryPairs().containsKey("userId"))
        {
            MembershipPersistence[] memberships = dataAccess.GetUserMemberships(
                    Long.parseLong(httpRequest.getQueryPairs().get("userId")));
            ArrayList<GroupDTO> groupDTOS = new ArrayList<>();
            for(MembershipPersistence m : memberships)
            {
                GroupPersistence group = dataAccess.GetGroup(m.getGroupId());
                groupDTOS.add(new GroupDTO(group.getName(), group.getId()));
            }
            String result = gson.toJson(groupDTOS);
            printWriter.println("HTTP/1.1 200 OK"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 1 &&
                httpRequest.getMethod().equals("POST"))
        {
            GroupDTO groupDTO = gson.fromJson(httpRequest.getBody(), GroupDTO.class);
            GroupPersistence groupPersistence = dataAccess.AddGroup(new GroupPersistence(groupDTO.getId(),groupDTO.getName()));
            GroupDTO outputDTO = new GroupDTO(groupPersistence.getName(), groupPersistence.getId());
            String result = gson.toJson(outputDTO);
            printWriter.println("HTTP/1.1 201 CREATED"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 3 &&
                httpRequest.getPathSegments()[2].equals("messages") &&
                httpRequest.getMethod().equals("POST"))
        {
            int groupId = Integer.parseInt(httpRequest.getPathSegments()[1]);
            MessageDTO messageDTO = gson.fromJson(httpRequest.getBody(), MessageDTO.class);
            MessagePersistence messagePersistence = dataAccess.AddMessage(new MessagePersistence(
                    messageDTO.getText(),
                    messageDTO.getTime(),
                    messageDTO.getUserId(),
                    groupId,
                    0));
            MessageDTO outputDTO = new MessageDTO(
                    messagePersistence.getText(),
                    messagePersistence.getUserId(),
                    messagePersistence.getTime());
            String result = gson.toJson(outputDTO);
            printWriter.println("HTTP/1.1 201 CREATED"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 2 &&
                httpRequest.getMethod().equals("GET"))
        {
            int groupId = Integer.parseInt(httpRequest.getPathSegments()[1]);
            GroupPersistence groupPersistence = dataAccess.GetGroup(groupId);
            GroupDTO groupDTO = new GroupDTO(groupPersistence.getName(),groupPersistence.getId());

            MembershipPersistence[] membershipPersistences = dataAccess.GetGroupMemberships(groupId);
            UserDTO[] userDTOS = new UserDTO[membershipPersistences.length];
            for(int i=0;i<membershipPersistences.length;i++)
            {
                userDTOS[i] = new UserDTO(null,null,membershipPersistences[i].getUserId());
            }
            groupDTO.setUsers(userDTOS);

            MessagePersistence[] messagePersistences = dataAccess.GetMessages(groupId);
            MessageDTO[] messageDTOS = new MessageDTO[messagePersistences.length];
            for(int i=0;i<messagePersistences.length;i++)
            {
                messageDTOS[i] = new MessageDTO(messagePersistences[i].getText(),
                        messagePersistences[i].getUserId(),
                        messagePersistences[i].getTime());
            }
            groupDTO.setMessages(messageDTOS);
            String result = gson.toJson(groupDTO);
            printWriter.println("HTTP/1.1 200 OK"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("users") &&
                httpRequest.getPathSegments().length == 1 &&
                httpRequest.getMethod().equals("POST"))
        {
            UserDTO userDTO = gson.fromJson(httpRequest.getBody(), UserDTO.class);
            UserPersistence userPersistence = dataAccess.AddUser(new UserPersistence(0,userDTO.getLogin(),userDTO.getName()));
            UserDTO outputDTO = new UserDTO(userPersistence.getLogin(), userPersistence.getName(), userPersistence.getId());
            String result = gson.toJson(outputDTO);
            printWriter.println("HTTP/1.1 201 CREATED"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 3 &&
                httpRequest.getPathSegments()[2].equals("users") &&
                httpRequest.getMethod().equals("POST"))
        {
            UserDTO userDTO = gson.fromJson(httpRequest.getBody(), UserDTO.class);
            long groupId = Long.parseLong(httpRequest.getPathSegments()[1]);

            MembershipPersistence membershipPersistence = dataAccess.AddMembership(
                    new MembershipPersistence(
                            0,
                            userDTO.getId(),
                            groupId));

            String result = gson.toJson(userDTO);
            printWriter.println("HTTP/1.1 201 Created"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();

        }
        else if(httpRequest.getPathSegments()[0].equals("users") &&
                httpRequest.getPathSegments().length == 1 &&
                httpRequest.getMethod().equals("PUT"))
        {
            UserDTO userDTO = gson.fromJson(httpRequest.getBody(), UserDTO.class);
            UserPersistence userPersistence = dataAccess.UpdateUser(new UserPersistence(userDTO.getId(),userDTO.getLogin(),userDTO.getName()));
            UserDTO outputDTO = new UserDTO(userPersistence.getLogin(), userPersistence.getName(), userPersistence.getId());
            String result = gson.toJson(outputDTO);
            printWriter.println("HTTP/1.1 200 OK"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        else if(httpRequest.getPathSegments()[0].equals("groups") &&
                httpRequest.getPathSegments().length == 1 &&
                httpRequest.getMethod().equals("PUT"))
        {
            // TODO
        }
        else if(httpRequest.getPathSegments()[0].equals("users") &&
                httpRequest.getPathSegments().length == 2 &&
                httpRequest.getMethod().equals("GET"))
        {
            String userLogin = httpRequest.getPathSegments()[1];
            UserPersistence userPersistence = dataAccess.GetUser(userLogin);
            UserDTO userDTO = new UserDTO(userPersistence.getLogin(), userPersistence.getName(), userPersistence.getId());
            String result = gson.toJson(userDTO);
            printWriter.println("HTTP/1.1 200 OK"); // Zapiš HTTP hlavičku
            printWriter.println("Content-type: text/json; charset=utf-8"); // Zapiš HTTP hlavičku
            printWriter.println(); // Ukonči hlavičku prázdnou řádkou
            printWriter.println(result);
            printWriter.close();
        }
        printWriter.close();
        socket.close();
    }
}
