package experiments.utils;

import java.util.LinkedHashMap;
import java.util.Map;

public class HttpRequest
{
    private final String method;
    private final String path;
    private final String[] head;
    private final String body;
    private final String[] pathSegments;

    private Map<String, String> queryPairs;

    public String getMethod()
    {
        return method;
    }

    public String getPath()
    {
        return path;
    }

    public String[] getHead()
    {
        return head;
    }

    public String getBody()
    {
        return body;
    }

    public HttpRequest(String firstLine, String[] head, String body, String method, String path, String[] pathSegments, Map<String, String> queryPairs)
    {
        this.head = head;
        this.body = body;
        this.method = method;
        this.path = path;
        this.queryPairs = queryPairs;
        this.pathSegments = pathSegments;
    }

    public Map<String, String> getQueryPairs()
    {
        return queryPairs;
    }

    public String[] getPathSegments()
    {
        return pathSegments;
    }
}
