package experiments.api;

public class GroupDTO
{
    // MEMBERS
    // MESSAGES

    public GroupDTO(String name, long id)
    {
        this.name = name;
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public long getId()
    {
        return id;
    }

    private String name;
    private long id;
    private UserDTO[] users;
    private MessageDTO[] messages;

    public UserDTO[] getUsers()
    {
        return users;
    }

    public void setUsers(UserDTO[] users)
    {
        this.users = users;
    }

    public MessageDTO[] getMessages()
    {
        return messages;
    }

    public void setMessages(MessageDTO[] messages)
    {
        this.messages = messages;
    }
}
