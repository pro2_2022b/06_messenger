package swingClient;

import experiments.api.ApiClient;
import experiments.api.GroupDTO;
import experiments.api.UserDTO;

import javax.swing.*;
import java.awt.*;

public class MessagesPanel extends JPanel
{
    private final ApiClient apiClient;
    private final ActiveGroupsTableModel activeGroupsTableModel;
    MessagesSubPanel messagesSubPanel = null;
    GroupDTO[] groups;
    private UserDTO user;

    public MessagesPanel(ApiClient apiClient)
    {

        this.apiClient = apiClient;
        setPreferredSize(new Dimension(800, 600));
        setLayout(new BorderLayout());

        activeGroupsTableModel = new ActiveGroupsTableModel(apiClient);
        JTable activeGroupsTable = new JTable(activeGroupsTableModel);
        activeGroupsTable.setPreferredSize(new Dimension(200,600));
        add(activeGroupsTable, BorderLayout.WEST);

        messagesSubPanel = new MessagesSubPanel(apiClient);
        add(messagesSubPanel, BorderLayout.CENTER);
        activeGroupsTable.getSelectionModel().addListSelectionListener(
                event -> {
                    if(activeGroupsTable.getSelectedRow() >= 0)
                    {
                        messagesSubPanel.reset(groups[activeGroupsTable.getSelectedRow()].getId(), user);
                    }
                });
    }

    public void reset(UserDTO user)
    {
        groups = apiClient.GetGroups(user.getId());
        this.user = user;
        activeGroupsTableModel.reset(groups);
    }
}
