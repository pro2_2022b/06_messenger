package swingClient;

import experiments.api.ApiClient;
import experiments.api.GroupDTO;
import experiments.api.MessageDTO;
import experiments.api.UserDTO;

import javax.swing.*;
import java.awt.*;
import java.time.Instant;

public class MessagesSubPanel extends JPanel
{
    private final ApiClient apiClient;
    private final MessagesTableModel messagesTableModel;
    private GroupDTO group;
    private UserDTO user;

    public MessagesSubPanel(ApiClient apiClient)
    {

        this.apiClient = apiClient;
        setPreferredSize(new Dimension(800, 600));
        setLayout(new BorderLayout());

        messagesTableModel = new MessagesTableModel();

        JTable messagesTable = new JTable(messagesTableModel);
        messagesTable.setPreferredSize(new Dimension(600,600));
        Color ivory = new Color(200, 200, 255);
        messagesTable.setOpaque(true);
        messagesTable.setFillsViewportHeight(true);
        messagesTable.setBackground(ivory);
        add(messagesTable,BorderLayout.CENTER);

        JPanel sendPanel = new JPanel();
        sendPanel.setLayout(new FlowLayout());
        add(sendPanel, BorderLayout.SOUTH);
        JTextField messageField = new JTextField("",60);
        JButton messageButton = new JButton("Poslat");
        messageButton.addActionListener(e->{
            apiClient.PostMessage(group.getId(),
                    new MessageDTO(messageField.getText(), user.getId(), Instant.now())
                    );
            SwingUtilities.invokeLater(()->{
                reset(group.getId(),user);
            });
        });
        sendPanel.add(messageField);
        sendPanel.add(messageButton);
    }

    public void reset(long groupId, UserDTO user)
    {
        this.group = apiClient.GetGroup(groupId);
        this.user = user;
        messagesTableModel.reset(group.getMessages());
    }
}
