package experiments.utils;

public class HttpResponse
{
    public HttpResponse(int code, String body)
    {
        this.code = code;
        this.body = body;
    }

    public int code;
    public String body;
}
