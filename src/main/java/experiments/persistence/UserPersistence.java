package experiments.persistence;

public class UserPersistence
{

    private long id;
    private String login;
    private String name;

    public UserPersistence(long id, String login, String name)
    {
        this.id = id;
        this.login = login;
        this.name = name;
    }

    public long getId()
    {
        return id;
    }

    public String getLogin()
    {
        return login;
    }

    public String getName()
    {
        return name;
    }

    public void setId(long id)
    {
        this.id = id;
    }
}
