package experiments.api;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import experiments.TimeAdapter;
import experiments.utils.HttpResponse;
import experiments.utils.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ApiClient
{
    private String baseUrl;
    private Gson gson;
    public ApiClient(String baseUrl) {
        this.baseUrl = baseUrl;
        GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Instant.class, new TimeAdapter());
        gson = gsonBuilder.create();
    }

    public String getTime()
    {
        throw new UnsupportedOperationException();
    }

    public GroupDTO PostGroup(GroupDTO body)
    {
        String json = gson.toJson(body);
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups","POST",json);
        GroupDTO result = gson.fromJson(response.body, GroupDTO.class);
        return result;
    }

    public MessageDTO PostMessage(long groupId, MessageDTO body)
    {
        String json = gson.toJson(body);
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups/"+groupId+"/messages","POST",json);
        MessageDTO result = gson.fromJson(response.body, MessageDTO.class);
        return result;
    }

    public GroupDTO GetGroup(long groupId)
    {
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups/"+groupId,"GET",null);
        GroupDTO result = gson.fromJson(response.body, GroupDTO.class);
        return result;
    }

    public GroupDTO[] GetGroups()
    {
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups","GET",null);
        GroupDTO[] result = gson.fromJson(response.body, GroupDTO[].class);
        return result;
    }

    public GroupDTO[] GetGroups(long userId)
    {
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups?userId="+userId,"GET",null);
        GroupDTO[] result = gson.fromJson(response.body, GroupDTO[].class);
        return result;
    }


    public UserDTO PostUser(UserDTO body)
    {
        String json = gson.toJson(body);
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"users","POST",json);
        UserDTO result = gson.fromJson(response.body, UserDTO.class);
        return result;
    }

    public UserDTO GetUser(String login)
    {
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"users/"+login,"GET",null);
        UserDTO result = gson.fromJson(response.body, UserDTO.class);
        return result;
    }

    public UserDTO PostGroupUser(long groupId, UserDTO body)
    {
        String json = gson.toJson(body);
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups/"+groupId+"/users","POST", json);
        UserDTO result = gson.fromJson(response.body, UserDTO.class);
        return result;
    }

    public UserDTO PutUser(UserDTO body)
    {
        String json = gson.toJson(body);
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"users","PUT",json);
        UserDTO result = gson.fromJson(response.body, UserDTO.class);
        return result;
    }

    public GroupDTO PutGroup(GroupDTO body)
    {
        String json = gson.toJson(body);
        HttpResponse response = Utils.GetHttpResponse(baseUrl+"groups","PUT",json);
        GroupDTO result = gson.fromJson(response.body, GroupDTO.class);
        return result;
    }
}
