package experiments.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Utils {
    public static HttpRequest ReadHttpRequest(Socket socket) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
        BufferedReader bufferedReader =
                new BufferedReader(inputStreamReader);
        String firstLine = bufferedReader.readLine();
        String[] split = firstLine.split(" ");
        System.out.println(firstLine);
        String line;
        ArrayList<String> head = new ArrayList<>();
        int contentLength = 0;
        do
        {
            line = bufferedReader.readLine();
            head.add(line);
            String[] splitLine = line.split(":");
            if(splitLine[0].equalsIgnoreCase("content-length"))
            {
                contentLength = Integer.parseInt(splitLine[1].trim());
            }
        }
        while (!line.equals(""));
        Map<String, String> queryPairs = new LinkedHashMap<String, String>();

        if(split[1].contains("?"))
        {
            String[] pairs = split[1].substring(split[1].indexOf('?')+1).split("&");
            for (String pair : pairs)
            {
                int idx = pair.indexOf("=");
                queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }
        }

        String[] pathSegments;
        int qIndex = split[1].indexOf("?");
        if(qIndex == -1)
        {
            pathSegments = split[1].substring(1).split("/");
        }
        else
        {
            pathSegments = split[1].substring(1,qIndex).split("/");
        }

        if (split[0].equals("GET")) {
            return new HttpRequest(firstLine, head.toArray(new String[0]), "", split[0], split[1], pathSegments, queryPairs);
        }
        char[] buffer = new char[contentLength];
        int stringLength = bufferedReader.read(buffer,0,contentLength);
        String body = String.valueOf(buffer,0, stringLength);
        return new HttpRequest(firstLine, head.toArray(new String[0]), body, split[0], split[1], pathSegments, queryPairs);
    }

    public static HttpResponse GetHttpResponse(String url, String method, String requestBody) {
        try {
            URL url1 = new URL(url);//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            conn.setRequestMethod(method);
            conn.setRequestProperty("Accept", "application/json");
            if(requestBody != null)
            {
                byte[] requestBodyBytes = requestBody.getBytes("utf-8");
                conn.setRequestProperty("Content-length", Integer.toString(requestBodyBytes.length));
                conn.setDoOutput(true);
                try (OutputStream os = conn.getOutputStream())
                {
                    os.write(requestBodyBytes, 0, requestBodyBytes.length);
                }
            }

            int responseCode = conn.getResponseCode();
            if (responseCode != 200 && responseCode != 201) {
                return new HttpResponse(responseCode, "");
            }

            java.util.Scanner s = new java.util.Scanner(conn.getInputStream()).useDelimiter("\\A");
            String body = s.hasNext() ? s.next() : "";
            return new HttpResponse(responseCode, body);
        } catch (IOException e) {
            return new HttpResponse(0, e.toString());
        }
    }
}
