package experiments.api;

public class MembershipDTO
{
    private String groupName;
    private String userName;
    private long groupId;
    private long userId;

    public String getGroupName()
    {
        return groupName;
    }

    public String getUserName()
    {
        return userName;
    }

    public long getGroupId()
    {
        return groupId;
    }

    public long getUserId()
    {
        return userId;
    }
}
