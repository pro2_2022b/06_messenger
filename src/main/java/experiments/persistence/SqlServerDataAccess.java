package experiments.persistence;

import java.sql.*;
import java.util.ArrayList;

public class SqlServerDataAccess implements DataAccess {

        Connection connection;
        PreparedStatement getGroupsStatement;
        PreparedStatement insertGroupStatement;
        PreparedStatement getGroupStatement;
        PreparedStatement getUserStatement;
        PreparedStatement insertUserStatement;
        PreparedStatement updateUserStatement;
        PreparedStatement getUserMembershipsStatement;
        PreparedStatement getGroupMembershipsStatement;
        PreparedStatement getMessagesStatement;
        PreparedStatement insertMessageStatement;

        PreparedStatement addMembershipStatement;
        public SqlServerDataAccess(String url, String login, String password) throws SQLException, ClassNotFoundException
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); // trik pro aktivování driveru
            connection = DriverManager.getConnection(url, login, password);
            getGroupsStatement = connection.prepareStatement("select [Id],[Name] from [Group]");
            insertGroupStatement = connection.prepareStatement("INSERT INTO [dbo].[Group]([Name]) VALUES(?)", Statement.RETURN_GENERATED_KEYS);
            getGroupStatement = connection.prepareStatement("select [Id],[Name] from [dbo].[Group] where Id=?");
            getUserStatement = connection.prepareStatement("select [Id],[Login],[Name] from [dbo].[User] where [Login]=?");
            insertUserStatement = connection.prepareStatement("INSERT INTO [dbo].[User]([Login],[Name]) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
            updateUserStatement = connection.prepareStatement("UPDATE [dbo].[User] set [Login]=?,[Name]=? where Id=?");
            getUserMembershipsStatement = connection.prepareStatement("select [Id],[UserId],[GroupId] from [dbo].[Membership] where UserId=?");
            getGroupMembershipsStatement = connection.prepareStatement("select [Id],[UserId],[GroupId] from [dbo].[Membership] where GroupId=?");
            getMessagesStatement = connection.prepareStatement("select [Id],[UserId],[GroupId],[Text],[Time] from [dbo].[Message] where GroupId=?");
            insertMessageStatement = connection.prepareStatement("INSERT INTO [dbo].[Message]([UserId],[GroupId],[Text],[Time]) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);updateUserStatement = connection.prepareStatement("UPDATE [dbo].[User] set [Login]=?,[Name]=? where Id=?");
            addMembershipStatement = connection.prepareStatement("insert into [dbo].[Membership]([UserId],[GroupId]) values(?,?)");
        }

        public GroupPersistence[] GetGroups()
        {
            ArrayList<GroupPersistence> results = new ArrayList<>();
            try
            {
                ResultSet resultSet= getGroupsStatement.executeQuery();
                while (resultSet.next())
                {
                    GroupPersistence row = new GroupPersistence(
                            resultSet.getLong("Id"),
                            resultSet.getString("Name"));
                    results.add(row);
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                return null;
            }
            return results.toArray(new GroupPersistence[0]);
        }

        public GroupPersistence AddGroup(GroupPersistence data)
        {
            try
            {
                insertGroupStatement.setString(1,data.getName());
                boolean res = insertGroupStatement.execute();
                ResultSet resultSet= insertGroupStatement.getGeneratedKeys();
                resultSet.next();
                long id =resultSet.getLong(1);
                data.setId(id);

            } catch (SQLException e)
            {
                e.printStackTrace();
            }

            return data;
        }

        public GroupPersistence GetGroup(long id)
        {
            try
            {
                getGroupStatement.setLong(1,id);
                ResultSet resultSet= getGroupStatement.executeQuery();
                resultSet.next();

                GroupPersistence row = new GroupPersistence(
                        resultSet.getLong("Id"),
                        resultSet.getString("Name"));
                return row;

            }
            catch (SQLException e)
            {
                e.printStackTrace();
                return null;
            }
        }

    @Override
    public UserPersistence GetUser(String login)
    {
        try
        {
            getUserStatement.setString(1,login);
            ResultSet resultSet= getUserStatement.executeQuery();
            resultSet.next();

            UserPersistence row = new UserPersistence(
                    resultSet.getLong("Id"),
                    resultSet.getString("Login"),
                    resultSet.getString("Name")
                    );
            return row;

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MembershipPersistence[] GetUserMemberships(long userId)
    {
        try
        {
            getUserMembershipsStatement.setLong(1,userId);
            ResultSet resultSet= getUserMembershipsStatement.executeQuery();
            ArrayList<MembershipPersistence> results = new ArrayList<>();
            while (resultSet.next())
            {
                MembershipPersistence row = new MembershipPersistence(
                        resultSet.getLong("Id"),
                        resultSet.getLong("UserId"),
                        resultSet.getLong("GroupId"));
                results.add(row);
            }

            return results.toArray(new MembershipPersistence[0]);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MembershipPersistence[] GetGroupMemberships(long groupId)
    {
        try
        {
            getGroupMembershipsStatement.setLong(1,groupId);
            ResultSet resultSet= getGroupMembershipsStatement.executeQuery();
            ArrayList<MembershipPersistence> results = new ArrayList<>();
            while (resultSet.next())
            {
                MembershipPersistence row = new MembershipPersistence(
                        resultSet.getLong("Id"),
                        resultSet.getLong("UserId"),
                        resultSet.getLong("GroupId"));
                results.add(row);
            }

            return results.toArray(new MembershipPersistence[0]);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MessagePersistence[] GetMessages(long groupId)
    {
        ArrayList<MessagePersistence> results = new ArrayList<>();
        try
        {
            getMessagesStatement.setLong(1,groupId);
            ResultSet resultSet= getMessagesStatement.executeQuery();
            while (resultSet.next())
            {
                MessagePersistence row = new MessagePersistence(
                        resultSet.getString("Text"),
                        resultSet.getTimestamp("Time").toInstant(),
                        resultSet.getLong("UserId"),
                        resultSet.getLong("GroupId"),
                        resultSet.getLong("Id")
                        );
                results.add(row);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return results.toArray(new MessagePersistence[0]);
    }

    @Override
    public UserPersistence AddUser(UserPersistence data)
    {
        try
        {
            insertUserStatement.setString(1,data.getLogin());
            insertUserStatement.setString(2,data.getName());
            boolean res = insertUserStatement.execute();
            ResultSet resultSet= insertUserStatement.getGeneratedKeys();
            resultSet.next();
            long id =resultSet.getLong(1);
            data.setId(id);

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public UserPersistence UpdateUser(UserPersistence data)
    {
        try
        {
            updateUserStatement.setString(1,data.getLogin());
            updateUserStatement.setString(2,data.getName());
            updateUserStatement.setLong(3,data.getId());
            boolean res = updateUserStatement.execute();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public MessagePersistence AddMessage(MessagePersistence data)
    {
        try
        {
            insertMessageStatement.setLong(1,data.getUserId());
            insertMessageStatement.setLong(2,data.getGroupId());
            insertMessageStatement.setString(3,data.getText());
            insertMessageStatement.setTimestamp(4,Timestamp.from(data.getTime()));
            boolean res = insertMessageStatement.execute();
            ResultSet resultSet= insertMessageStatement.getGeneratedKeys();
            resultSet.next();
            long id =resultSet.getLong(1);
            data.setId(id);

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public MembershipPersistence AddMembership(MembershipPersistence data)
    {
        try {
            addMembershipStatement.setLong(1,data.getUserId());
            addMembershipStatement.setLong(2,data.getGroupId());
            addMembershipStatement.execute();
            return data;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
