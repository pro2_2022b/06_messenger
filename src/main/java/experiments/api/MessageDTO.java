package experiments.api;
import java.time.Instant;

public class MessageDTO
{
    String text;
    long userId;
    Instant time;

    public MessageDTO(String text, long userId, Instant time)
    {
        this.text = text;
        this.userId = userId;
        this.time = time;
    }

    public String getText()
    {
        return text;
    }

    public long getUserId()
    {
        return userId;
    }

    public Instant getTime()
    {
        return time;
    }
}
