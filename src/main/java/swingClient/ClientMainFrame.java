package swingClient;
import experiments.api.ApiClient;
import experiments.api.GroupDTO;
import experiments.api.UserDTO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class ClientMainFrame extends JFrame
{
    private final ApiClient apiClient;
    UserGroupsTableModel membershipsTableModel;
    JTabbedPane userTabsPane;
    UserDTO user;
    JLabel infoLabel;
    EditUserPanel editUserPanel;
    private MessagesPanel messagesPanel;
    private FriendsTableModel friendsTableModel;

    public ClientMainFrame(ApiClient apiClient) throws IOException
    {
        this.apiClient = apiClient;
        Init();
    }

    private void Init()
    {

        setTitle("PRO2 Messenger");
        setVisible(true);
        setBackground(Color.white);
        setSize(600, 800);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JPanel loginPanel = new JPanel();
        JPanel userPanel = new JPanel();
        JPanel userOptionsPanel = new JPanel();
        loginPanel.setLayout(new BoxLayout(loginPanel,BoxLayout.X_AXIS));
        userPanel.setLayout(new BoxLayout(userPanel,BoxLayout.Y_AXIS));
        userOptionsPanel.setLayout(new BoxLayout(userOptionsPanel,BoxLayout.X_AXIS));
        userOptionsPanel.setAlignmentX(LEFT_ALIGNMENT);

        add(loginPanel, BorderLayout.NORTH);
        add(userPanel, BorderLayout.CENTER);

        userTabsPane = new JTabbedPane();
        userTabsPane.setVisible(false);

        JPanel allGroupsPanel = new JPanel();
        allGroupsPanel.setLayout(new BoxLayout(allGroupsPanel,BoxLayout.Y_AXIS));
        JButton joinButton = new JButton("Vstoupit");
        allGroupsPanel.add(joinButton);

        userPanel.add(userTabsPane);
        userTabsPane.addTab("Všechny skupiny", allGroupsPanel);
        userPanel.add(userOptionsPanel);
        JTextField loginTextField = new JTextField();

        JButton loginButton = new JButton("PŘIHLÁSIT");
        JButton signUpButton = new JButton("REGISTROVAT");
        loginPanel.add(new JLabel("Login:"));
        loginPanel.add(loginTextField);
        loginPanel.add(loginButton);
        loginPanel.add(signUpButton);
        loginButton.addActionListener(e -> {
            Thread thread= new Thread(()-> {
                user = apiClient.GetUser(loginTextField.getText());
                LoadUser();
            });
            thread.start();
        });
        signUpButton.addActionListener(e -> {
            Thread thread= new Thread(()-> {
                user = apiClient.PostUser(new UserDTO(
                        loginTextField.getText(),
                        "",
                        0));
                LoadUser();
            });
            thread.start();
        });

        infoLabel = new JLabel();
        userOptionsPanel.add(infoLabel);
        infoLabel.setPreferredSize(new Dimension(200, 20));

        membershipsTableModel = new UserGroupsTableModel();
        JTable membershipsTable = new JTable(membershipsTableModel);
        allGroupsPanel.add(membershipsTable);

        joinButton.addActionListener(e->{
            Thread thread = new Thread(()->{
                apiClient.PostGroupUser(
                        Integer.parseInt(membershipsTableModel.getValueAt(membershipsTable.getSelectedRow(),1).toString()),
                        user
                        );
            });
            thread.start();
        });

        editUserPanel = new EditUserPanel(apiClient);
        userTabsPane.addTab("Upravit profil", editUserPanel);

        messagesPanel = new MessagesPanel(apiClient);
        userTabsPane.addTab("Zprávy", messagesPanel);

        friendsTableModel = new FriendsTableModel(apiClient);
        JTable friendsTable = new JTable(friendsTableModel);
        friendsTable.getColumnModel().getColumn(0).setHeaderValue("Id");
        friendsTable.getColumnModel().getColumn(1).setHeaderValue("Common groups");
        friendsTable.getColumnModel().getColumn(2).setHeaderValue("Received messages");
        JScrollPane friendsPanel = new JScrollPane();
        friendsPanel.setViewportView(friendsTable);
        userTabsPane.addTab("Přátelé", friendsPanel);
    }
    private void LoadUser()
    {
        try
        {
            userTabsPane.setVisible(true);

            infoLabel.setText("Přihlášen: "+user.getLogin());
            infoLabel.setOpaque(true);
            infoLabel.setBackground(Color.green);
            GroupDTO[] groups = apiClient.GetGroups();

            SwingUtilities.invokeLater(()->{
                membershipsTableModel.reset(groups);
            });

            SwingUtilities.invokeLater(()->{
                editUserPanel.reset(user);
            });

            SwingUtilities.invokeLater(()->{
                messagesPanel.reset(user);
            });

            SwingUtilities.invokeLater(()->{
                friendsTableModel.reset(user);
            });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}