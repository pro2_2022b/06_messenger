package swingClient;

import experiments.api.GroupDTO;
import experiments.api.MessageDTO;

import javax.swing.table.AbstractTableModel;

public class MessagesTableModel extends AbstractTableModel
{

    private MessageDTO[] messages = new MessageDTO[0];

    public void reset(MessageDTO[] messages)
    {

        this.messages = messages;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return messages.length;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        switch (columnIndex)
        {
            case 0: return messages[rowIndex].getUserId();
            case 1: return messages[rowIndex].getTime();
            case 2: return messages[rowIndex].getText();
            default: return "";
        }
    }
}
