package swingClient;

import experiments.api.GroupDTO;

import javax.swing.table.AbstractTableModel;

public class UserGroupsTableModel extends AbstractTableModel
{

    private GroupDTO[] groups = new GroupDTO[0];

    public void reset(GroupDTO[] groups)
    {

        this.groups = groups;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return groups.length;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        switch (columnIndex)
        {
            case 0: return groups[rowIndex].getName();
            case 1: return groups[rowIndex].getId();
            default: return "";
        }
    }
}
