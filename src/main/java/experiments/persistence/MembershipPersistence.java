package experiments.persistence;

public class MembershipPersistence
{
    private final long id;
    long userId;
    long groupId;

    public MembershipPersistence(long id, long userId, long groupId)
    {
        this.id = id;
        this.userId = userId;
        this.groupId = groupId;
    }

    public long getUserId()
    {
        return userId;
    }

    public long getGroupId()
    {
        return groupId;
    }
}
