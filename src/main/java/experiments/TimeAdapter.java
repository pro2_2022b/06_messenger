package experiments;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeAdapter extends TypeAdapter
{
    @Override
    public void write(JsonWriter jsonWriter, Object o) throws IOException
    {
        String s = ((Instant)o).toString();
        jsonWriter.value(s);
    }

    @Override
    public Object read(JsonReader jsonReader) throws IOException
    {
        String s = jsonReader.nextString();
        return Instant.parse(s);
    }
}
